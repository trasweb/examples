<?php
if ( !defined('ABSPATH') ) die('¡ Hello, World ! ');

define('CUANDO_PLUGINS_Y_TEMAS_ESTEN_CARGADOS',  'wp_loaded');
define('SALTOS_DE_LINEAS',                       '/[\n\r]/');
define('COMENTARIOS_DE_DESARROLLADORES',         '/<!--\s.*?-->/');
define('MAS_DE_DOS_ESPACIOS_EN_BLANCO',          '/\s{2,}/s');
define('ESPACIO_EN_BLANCO',                      ' ');

/*
 * Plugin Name:       Minimiza el HTML ( WPO )
 * Plugin URI:        https://trasweb.net/blog/wpo/entender-la-minimizacion-html-y-usarla-bajo-wordpress
 * Description:       Mejora el rendimiento de la web, quitando c&oacute;digo innecesario del HTML
 * Version:           0.0.1
 * Author:            Manuel Canga / Trasweb
 * Author URI:        https://trasweb.net
 * License:           GPL
 */


add_action(CUANDO_PLUGINS_Y_TEMAS_ESTEN_CARGADOS, 've_guardando_el_HTML');

function ve_guardando_el_HTML() {
    ob_start('minimiza_el_HTML');
}

function minimiza_el_HTML($HTML_generado_por_WordPress) {

  $a_quitar = [ SALTOS_DE_LINEAS, COMENTARIOS_DE_DESARROLLADORES, MAS_DE_DOS_ESPACIOS_EN_BLANCO ];       
  $reemplazar_por = ESPACIO_EN_BLANCO;

  return filtra_el_HTML($a_quitar, $reemplazar_por, $HTML_generado_por_WordPress);

}

function filtra_el_HTML($a_quitar, $reemplazar_por, $HTML_generado_por_WordPress) {
   return preg_replace($a_quitar, $reemplazar_por, $HTML_generado_por_WordPress);
}
