<?php
if ( !defined('ABSPATH') ) die('¡ Hello, World ! ');

/*
 * Plugin Name:       Más velocidad sin sidebar ( WPO )
 * Plugin URI:        https://trasweb.net/blog/wpo/haz-la-version-movil-de-tu-web-con-wordpress-mas-rapida-sin-sidebars
 * Description:       Aumenta la velocidad de un sitio móvil al quitarle los sidebars/paneles.
 * Version:           0.0.1
 * Author:            Manuel Canga
 * Author URI:        https://tasweb.net
 * License:           GPL
 */

add_action( 'plugins_loaded',function() {
    if(!is_admin() && wp_is_mobile() ) {
        add_filter('sidebars_widgets', '__return_empty_array' );
    }
});
