<?php
if ( !defined('ABSPATH') ) die('¡ Hello, World ! ');


/*
 * Plugin Name:       Recorta dominio de recursos ( WPO )
 * Plugin URI:        https://trasweb.net/blog/wpo/reduce-mas-de-15-bytes-por-cada-imagen-u-otro-recurso-en-tu-wordpress
 * Description:       Recorta el dominio de las urls de los recursos en WordPress
 * Version:           0.0.1
 * Author:            Manuel Canga / Trasweb
 * Author URI:        https://trasweb.net
 * License:           GPL
 */


function dominio_wordpress() {
    static $dominio = null;

    if(isset($dominio) ) {
        return $dominio;
    }

    $url = parse_url(site_url());

    return $url['scheme'].'://'.$url['host'];
}

add_filter( 'upload_dir', function ( $datos_del_recurso ) {

  elimina_dominio($datos_del_recurso['url']);
  elimina_dominio($datos_del_recurso['baseurl']);

  return $datos_del_recurso;
});


function elimina_dominio(& $url_recurso) {
   $url_recurso = str_replace(dominio_wordpress(), '', $url_recurso);
}


/** ---- EN LOS RSS DEJAMOS TODO IGUAL ---- */

add_filter( 'the_excerpt_rss', 'en_rss_recursos_con_dominio');
add_filter( 'the_content_feed', 'en_rss_recursos_con_dominio');

function en_rss_recursos_con_dominio($contenido_RSS) {
   return str_replace('src="/', 'src="'.dominio_wordpress().'/', $contenido_RSS);
};