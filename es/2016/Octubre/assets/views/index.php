<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8 />
	<title>Demo de gesti�n de Assets</title>

	<?php
		//A�adimos los archivos css
		$css_files = Assets::getCssFiles('top');
		foreach($css_files as $_file) {
			echo '<link rel="stylesheet" type="text/css" media="screen" href="'. $_file.'" />';
		}
	?>

</head>
<body>
	<h1>Hola Mundo</h1>
	
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium <a href='https://trasweb.net'>eu est non lobortis</a>. In tincidunt sagittis metus, nec euismod quam. Nunc at felis arcu. Sed id felis metus. Pellentesque et congue justo, quis tempus urna. Integer tincidunt congue massa, eget efficitur enim pretium vitae. Sed mattis ac ipsum quis fermentum. Integer purus metus, congue ac ante sit amet, vehicula mollis neque. Nullam maximus turpis diam, et convallis erat pharetra quis. Integer sed mi nulla. Mauris vitae dapibus augue. Suspendisse facilisis euismod est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam ac condimentum velit, quis dictum quam. Nulla dignissim, lacus at ullamcorper elementum, tortor quam rutrum leo, non tincidunt arcu erat a quam. Curabitur rutrum orci molestie augue faucibus laoreet.</p>

    <p>Aenean vehicula porttitor blandit. Nunc tempor porttitor orci a pulvinar. Mauris in congue mauris. Nulla quis enim non tortor rhoncus condimentum. Sed nec mauris nec libero pellentesque venenatis. Nunc quis ante ante. Mauris suscipit, sapien vel condimentum aliquam, mi purus sollicitudin metus, non porttitor turpis diam quis elit.</p>

    <p>Sed et euismod libero. Pellentesque condimentum tellus sit amet justo scelerisque convallis nec quis lectus. Nullam a scelerisque diam. Nulla condimentum aliquet diam, ac fringilla leo condimentum in. In risus sem, ullamcorper sit amet ante non, hendrerit mattis augue. Phasellus blandit egestas arcu sed malesuada. Suspendisse semper dolor sed lacus interdum vulputate. Proin ac consectetur arcu. Integer suscipit risus eu tellus tincidunt sollicitudin. Sed eget nunc semper diam aliquam vulputate. Vivamus orci nisi, venenatis ac cursus et, dapibus tempor velit. Mauris hendrerit iaculis tempor. Maecenas ut massa ante.</p>

</body>
</html>
