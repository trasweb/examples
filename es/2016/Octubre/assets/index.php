<?php
/**
 Ejemplo de como gestionar recursos CSS sin tanta Tool
 @link https://trasweb.net/blog/desarrollo-a-medida/como-gestionar-archivos-css-sin-tanta-tool
 @author Manuel Canga
 @license GPLv2
*/


define('PROJECT', __DIR__);
define('CONFIG', PROJECT.'/site.conf'); 

include(PROJECT.'/lib/Assets.php');

/* Lo más conveniente sería que hubiera un panel de control y que guardara las opciones del usuario como colores, tipos de letra, etc
   Luego, recuperar esos datos desde la base de datos o dónde quiera que estuvieran
   Ahora se añade código css personalizado dentro del archivo .ini pero también podría añadirse una zona en el panel, con un editor, para escribir el código css personalizado
*/
$config_vars = [];
$hay_configuracion = file_exists(CONFIG);
if($hay_configuracion ) {
	$config_vars = parse_ini_file(CONFIG);
}


Assets::addCss('style', $config_vars);

include(PROJECT.'/views/index.php');



