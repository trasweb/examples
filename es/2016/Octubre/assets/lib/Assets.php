<?php
/**
  Clase para gestionar assets se sirve de archivos de configuración .ini
  y de less( http://leafo.net/lessphp/ )
*/
include_once(PROJECT.'/lib/lessphp/lessc.inc.php');

class Assets {
  static private $css_files = [];

  const CSS_PATH = '/css/';


  /**
	Devuelve la última vez que se modificó file.
    Si file no existe, se devuelve 0
  */
  static private function getModificationTime(string $file): int{
	$file = PROJECT.$file;

	$archivo_existe = file_exists($file);
	if(!$archivo_existe) return 0;

     return filemtime($file);
  }


  static function addCss(string $_file, array $vars = [], string $place = 'top'){
	$_file = str_replace('.css','', $_file);

	/* 
		A partir del archivo $_file( example ) crearemos un archivo less minimizado de nombre igual que $_file pero extension min.css ( example.min.css )
    */
     $main_file  =  self::CSS_PATH.$_file.'.css';
     $out_file  = self::CSS_PATH.$_file.'.min.css';


	$ult_modificacion_css_main = self::getModificationTime($main_file);

    //Todo se basa en un archivo css existente, si no existe, no tiene sentido seguir
	$no_existe_archivo_principal =  !$ult_modificacion_css_main;
	if($no_existe_archivo_principal) return false;

	$ult_modificacion_out_file  = self::getModificationTime($out_file);

	$necesita_generacion = $ult_modificacion_css_main > $ult_modificacion_out_file; 
	if($necesita_generacion) {
		$everything_ok = self::parseCss($main_file , $out_file, $vars );
		if(!$everything_ok) {
			return false;
		}
	}

	self::$css_files[$place][] = $out_file;
  }

	/**
		Parseamos el código less de $main_file atendiendo a las variables $vars
	*/
	static function parseCss($main_file, $out_file, $vars) {
		$content = file_get_contents(PROJECT.$main_file);

		try {
			$less = new \lessc();
			$less->setFormatter("compressed");
			$less->setImportDir(self:: CSS_PATH);
			if(isset($vars['custom_css']) ) {
				$content .= $vars['custom_css'];
				unset($vars['custom_css']);
			}

			
			$content = $less->parse($content, $vars );

		}catch(Exception $e) {
			error_log("Assets fatal error: ".$e->getMessage() );
			return false;
		}

	   file_put_contents(PROJECT.$out_file, $content);

		return true;
	}

	static function getCSSFiles(string $place) {

		return self::$css_files[$place]?? [];
	}
}
