<?php

/**
Routing inversa simple.
@see https://trasweb.net/blog/desarrollo-a-medida/simple-routeador-inverso
@param string $_url patrón con molde url 
@param array  $_params posibles parámetros para incluir al patrón url
*/

function reverse_routing($_url, array $_params = [] ) {
    
		//Procesamos las variables
        $buscar_vars='@:([a-zA-Z0-9]+)?@';
		$url = preg_replace_callback($buscar_vars, function($matches) use($_params) {
			//matches[0] con puntos
			//matches[1] sin puntos;
			
			if(isset($matches[1]) ) {
				$var = $matches[1];
                $hay_valor_para_var = isset($_params[$var]);
				if($hay_valor_para_var) ) {
					return $_params[$var];
				}else {
					return '@'; //not value for this var
				}
			}
			
		}, $_url);
		
		//Eliminamos todos los elementos corchetes
        $busca_corchetes='%\\[(?:([^\\[\\]]*)|\[(?R)\])\\]%';
		do {
			$url = preg_replace_callback($busca_corchetes, function($matches) {
                //matches[0] con corchetes
				//matches[1] sin corchetes;
                $hubo_dentro_corchetes_parametro_sin_valor = strpos($matches[1], '@') !== false;
				if( $hubo_dentro_corchetes_parametro_sin_valor ) {
					return ''; //Si lleva arroba es porque un elemento se quedo cojo, por tanto, no devolvemos nada.
				}else {	
					return $matches[1]; //Devolvemos todo tal cual( sin los corchetes )
				}

			}, $url, -1, $count);

		}while( $count != 0);

		//Quitamos el rastro de @ en el string
		return str_replace('@', '', $url);
}