<?php

/** Función con eñe */
function ñ(...$tonterías) {
    echo implode(PHP_EOL, $tonterías).PHP_EOL;
}

ñ('llamando', 'a', 'la', 'función', 'eñe');

/** Constante con eñes */
const CAÑÓN = 'Dicese persona cañona';
ñ(CAÑÓN);

/** Variable con acento */
$canción = 'Ojos de gata';
echo ñ($canción);




