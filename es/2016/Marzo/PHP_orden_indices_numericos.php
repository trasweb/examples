<?php

//Aquí el orden de los índices lo marca el orden de inserción
$array = [2 => 1, 1 => 2, 5 => 3];

print_r($array);

//Aquí el orden de los índices lo asigna una función externa
$array = [1, 2,3];

print_r(array_reverse($array, $preserver_key = true));

//Estos arrays son iguales pero no son estrictamente iguales por no tener el mismo orden de índices.
$array1 = [1 =>1,2 => 2,3 => 3];
$array2 = [3 => 3, 2 => 2, 1 => 1];

echo "Iguales:".($array1 == $array2).PHP_EOL; //1
echo "Iguales:".($array1 === $array2).PHP_EOL; //
