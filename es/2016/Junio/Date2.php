 <?php 

    /** Podemos convertir tambien entre formatos
        \trasweb\Date::transform( fecha_a_transformar, formato_de_fecha, nuevo_formato_fecha );
    */
    echo  \trasweb\Date::transform('20/07/2010', 'fecha', 'database'); //2010-07-20 00:00:00
    echo  \trasweb\Date::transform('20/07/2010', 'fecha', 'mes'); //10
    echo \trasweb\Date::transform("2010-07-20 00:00:00", 'database', 'fecha'); //20/07/2010
    echo \trasweb\Date::transform("2010-07-20 00:00:00", 'database', 'cookie'); //Tuesday, 20-Jul-2010 00:00:00 CEST
    echo \trasweb\Date::transform('10/10/2020 10:11:12', 'fechahora', 'rss'); //Sat, 10 Oct 2020 10:11:12 +0200
    echo \trasweb\Date::transform('10/10/2020 10:11:12', 'fechahora', 'mes'); //10
    echo \trasweb\Date::transform('10/10/2020 10:11:12', 'fechahora', 'month'); //10
    echo   \trasweb\Date::transform('10/10/2020 10:11:12', 'fechahora', 'monthname'); //octubre

