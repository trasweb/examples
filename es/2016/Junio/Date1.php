<?php 

    /** Podemos obtener la fecha actual en distintos formatos
        \trasweb\Date::current( formato_salida_fecha );
    */

    echo \trasweb\Date::current(); // 06/12/2016
    echo \trasweb\Date::current('mysql'); // 2016-06-12 20:54:42
    echo \trasweb\Date::current('fecha'); // 12/06/2016
    echo \trasweb\Date::current('date'); // 06/12/2016
    echo \trasweb\Date::current('datetime'); // 06/12/2016 20:54:42
    echo \trasweb\Date::current('dia'); // 12
    echo \trasweb\Date::current('timestamp'); // 1465757682
