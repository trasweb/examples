<?php 

    /** Podemos tambien hacer operaciones con una fecha
       \trasweb\Date::change( cambio, fecha, formato_fecha-entrada[, formato_fecha_salida ] );
    */
    echo  \trasweb\Date::change('+1 Month', '20/07/2010', 'fecha');  //20/08/2010
    echo    \trasweb\Date::change('+1 Month', '20/07/2010', 'fecha', 'nombremes'); //agosto

    echo   \trasweb\Date::change('+3 Years', '20/07/2010', 'fecha');  //20/07/2013
    echo  \trasweb\Date::change('+3 Years', '20/07/2010', 'fecha', 'year');   //13

    echo   \trasweb\Date::change('+3 Days', '2000-07-10', 'database-date');   //2000-07-13
    echo trasweb\Date::change('+3 Days', '2000-07-10', 'database-date','nombredia'); //jueves


    /** Tambien se pueden hacer cambios desde la fecha actual
    \trasweb\Date::get( cambio,formato_salida =  'timestamp', timestamp_desde_el_qu e_hacer_cambios = null] );
    */

    echo  \trasweb\Date::get('+1 Month', 'fecha');  //12/07/2016
    echo    \trasweb\Date::get('+1 Month','nombremes'); //julio

    echo   \trasweb\Date::get('+3 Years', 'fecha');  //12/06/2019
    echo  \trasweb\Date::get('+3 Years', 'year');   //19

    echo   \trasweb\Date::get('+3 Days',  'database-date');   //2016-06-15
    echo trasweb\Date::get('+3 Days', 'nombredia'); //miércoles
