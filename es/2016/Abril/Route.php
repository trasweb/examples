<?php

/**
Clase simple de routeo
@see https://trasweb.net/blog/desarrollo-a-medida/simple-routeador-para-php
*/
Class Route {

    private $urlToCheck = null;
    
    function __construct($_url = null) {
       //Normalmente la url a verificar será la del navegador.
      //Sin embargo, damos la opcion de personalizarla pasando un string al instanciar la clase
       $url_a_usar = $_url?? $_SERVER['REQUEST_URI'];
       $url_con_solo_path = parse_url( $url_a_usar , PHP_URL_PATH);
       $url_sin_caracteres_extras = rawurldecode($url_con_solo_path);
       $this->urlToCheck = '/'. trim($url_sin_caracteres_extras, '/');
    }

  /**
     Hacemos una comparación de la url 
  */
   function procesar($_url_pattern, &$_matches = []) {
        //vamos a convertir poco a poco la cadena pasada en una cadena con expresiones regulares PCRE
   
       //Procesamos el metacaracter ilimitado
      //Ejemplo /noticias/~/:pagina  calzaría con /noticias/listado/destacados/10 y daría como resulado ['pagina' => 10]
  		 $url_con_metacaracter_ilimitado = preg_replace('@~@', '(.*?)',  $_url_pattern);

		  //Elementos opcionales /[:prueba]/  o /noticias/[list]/ o /noticias[-listado]/
 	     $url_con_elementos_opcionales = preg_replace('@\\[(.*)\\]@U', '($1)?',  $url_con_metacaracter_ilimitado );

	   	//Elementos con parámetro de tipo i-nt
 	  	 $url_con_ints = preg_replace('@i:([\w\-]+)@', '(?<\1>\d+)',  $url_con_elementos_opcionales);

		  //Parámetros generales:  /noticias/:response/:id
 	  	 $reg_expresion = $url_con_param_generales = preg_replace('@:([\w]+)@', '(?<\1>[^/]+)',  $url_con_ints);

		 $_matches = [];


		 if( !preg_match("@^{$reg_expresion}$@", $this->urlToCheck, $_matches) ) {
			return false;
		}else {
		  //Sólo devolvemos al programador cliente los indices alfanumerico. No nos interesa los numéricos.
			$_matches =  array_filter($_matches, function($value, $key) {return !is_int($key);}, ARRAY_FILTER_USE_BOTH);
			return true;
		}
   }
   
  /**
    @param string $_url_pattern es la url con nuestros parámetros
    @param array $others, parámetros por defecto para añadir si encaja la url pasada(_url_pattern ) con la del navegador
    @param callable $callback es un callback que se usará para personalizar los datos o para hacer la operacion que queramos.
  */
  function fromPattern($_url_pattern = '', array $_others = [], callable $_callback = null) {
		 if( $this->procesar($_url_pattern, $new_args) ) {
		 	$args = $new_args + $_others;
			if(isset($_callback) ) {
				$result = $_callback($args);
				
				if(isset($result) && is_array($result) ) {
					$args = $result;
				}
			}
 		 }

		return $args;
	}


}