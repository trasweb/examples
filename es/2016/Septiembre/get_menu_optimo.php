<?php
/** 
Ejemplo pàra post: https://trasweb.net/blog/optimizacion-web/optimizacion-de-funciones-php
Ejecutable desde: https://3v4l.org/r8MDJ
*/

$vista = new stdclass;
//...
$vista->es_portada = false;
//...
function get_menu() {
    
    //Las variables estáticas ya no tendrían mucho sentido
    static $cache = null;
    if(isset($cache) ) return yield from $cache;
    
    error_log('Se inicia la creación de menú');
    
    $i = 0;
    $menu[$i] = ['title' => 'Inicio', 'href' => '/'];
    yield $i => $menu[$i]; $i++;
    $menu[$i] = ['title' => 'Servicios', 'href' => '/servicios'];
    yield $i => $menu[$i]; $i++;
    $menu[$i]= ['title' => 'Quiénes somos', 'href' => '/quienes-somos'];
    yield $i => $menu[$i]; $i++;
    $menu[$i]=  ['title' => 'Blog', 'href' => '/blog'];
    yield $i => $menu[$i]; $i++;
    $menu[$i]  = ['title' => 'Contacto', 'href' => '/contacto'];
    yield $i => $menu[$i]; $i++;
 
    error_log('Se finaliza la creación de menú');
    $cache = $menu;
    
    return $cache;
}


$vista->menu = get_menu();


//_______ En las vista ____________

if(!$vista->es_portada) {
  echo '<ul>';
  foreach($vista->menu as $menuitem) {
      echo '<li><a href="'.$menuitem['href'].'">'.$menuitem['title'].'</a></li>';
  }
  echo '</ul>';
  
  //Se podría hacer lo mismo en el pie para el menú del pie
  $menu =  $vista->menu->getReturn();
  echo '<ul>';
  foreach($menu as $menuitem){
        echo '<li><a href="'.$menuitem['href'].'">'.$menuitem['title'].'</a></li>';
  }
    echo '</ul>';
}