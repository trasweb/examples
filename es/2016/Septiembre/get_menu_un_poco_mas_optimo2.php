<?php
/** 
Ejemplo pàra post: https://trasweb.net/blog/optimizacion-web/optimizacion-de-funciones-php
Se puede ver la ejecución en: https://3v4l.org/Ogl5t
*/

$vista = new stdclass;
//...
$vista->es_portada = false;
//...
function get_menu() {
    
    static $cache = null;
    if(isset($cache) ) return $cache;
    
    error_log('Se inicia la creación de menú');
    
    $menu[] = ['title' => 'Inicio', 'href' => '/'];
    $menu[] = ['title' => 'Servicios', 'href' => '/servicios'];
    $menu[] = ['title' => 'Quiénes somos', 'href' => '/quienes-somos'];
    $menu[] = ['title' => 'Blog', 'href' => '/blog'];
    $menu[] = ['title' => 'Contacto', 'href' => '/contacto'];
 
    error_log('Se finaliza la creación de menú');
    
    return $cache = $menu;   
}


$vista->menu = get_menu();


//_______ En la vista ____________

if(!$vista->es_portada) {
  echo '<ul>';
  foreach($vista->menu as $menuitem) {
      echo '<li><a href="'.$menuitem['href'].'">'.$menuitem['title'].'</a></li>';
  }
  echo '</ul>';
  
  //Se podría hacer lo mismo en el pie para el menú del pie
}