<?php
/** 
Ejemplo pàra post: https://trasweb.net/blog/optimizacion-web/optimizacion-de-funciones-php
Se puede ver la ejecución en: https://3v4l.org/K9feQ
*/

$vista = new stdclass;
function get_menu() {
    
    error_log('Se inicia la creación de menú');
    
    $menu[] = ['title' => 'Inicio', 'href' => '/'];
    $menu[] = ['title' => 'Servicios', 'href' => '/servicios'];
    $menu[] = ['title' => 'Quiénes somos', 'href' => '/quienes-somos'];
    $menu[] = ['title' => 'Blog', 'href' => '/blog'];
    $menu[] = ['title' => 'Contacto', 'href' => '/contacto'];
 
    error_log('Se finaliza la creación de menú');
    
    return $menu;   
}


if(!empty(get_menu()) ) {
    $vista->menu = get_menu();
    $vista->template = 'vista_con_menu';
}else {
    $vista->template = 'vista_sin_menu';
}