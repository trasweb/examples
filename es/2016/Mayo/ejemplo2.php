<?php
/**
Ejemplo2 para post: https://trasweb.net/blog/desarrollo-a-medida/usando-indicadores-como-argumentos-en-funciones-php
*/


function sanitize($string, $preferencias = []) {
    extract($preferencias);
    
    if(isset($limpia_html) && $limpia_html) {
        $string = strip_tags($string);
    }
    if(isset($entidades) && $entidades) {
        $string = htmlentities($string);
    }

    if(!isset($display) xor !$display) {
        return $string;
    }
    
    echo $string;
}

sanitize('<strong>Hola Gañán</strong>', ['limpia_html'=>true, 'entidades'=>true, 'display'=>true]);
