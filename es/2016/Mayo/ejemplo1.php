<?php
/**
Ejemplo1 para post: https://trasweb.net/blog/desarrollo-a-medida/usando-indicadores-como-argumentos-en-funciones-php
*/

function sanitize($string, $limpia_html=false, $entidades=false, $display=false) {
    if($limpia_html) {
        $string = strip_tags($string);
    }
    if($entidades) {
        $string = htmlentities($string);
    }
    
    if(!$display) {
        return $string;
    }
    
    echo $string;
}

sanitize('<strong>Hola Gañán</strong>', $limpia_html=true, $entidades=true, $display=true);