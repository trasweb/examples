<?php

/**
 Añade archivos CSS en webs usando el sistema de child themes de WordPress
 @see https://trasweb.net/blog/optimizacion-web/estas-creando-mal-tus-temas-hijos-de-wordpress
 @author Manuel Canga
 @license GPLv2
*/

//Para añadir en el functions.php del hijo.
function trasweb_add_css($_file, $place = 'hijo', $admin_zone = false) {
  if(is_admin() && !$admin_zone) return ;

  //Ya que los archivos del child theme están localizado en una ubicación diferente a la del padre
  //es necesario, según si el css es del padre o del hijo, tomar la base correcta
  $base = ('hijo' == $place)?  get_stylesheet_directory_uri() : get_template_directory_uri();
  
  $file = $base.$_file;
  
  //La prioridad de un css de un tema hijo es más baja que la del padre, es decir, se carga después
  //Así podrá sobrescribir los estilos del padre. 
  $priority = ('hijo' == $place)? 30 : 15;
   
  //Creamos un identificador del archivo css al vuelo.  
  //Un identificador es útil, entre otras cosas, para evitar duplicados o para eliminar estilos por filtros
  $idcss = $place.'-'.basename($_file, '.css');
  
  //Hacemos que se encole.
  add_action( 'wp_enqueue_scripts', function() use($file, $idcss) {
     wp_enqueue_style( $idcss, $file );
  }, $priority);
  

}

//primero añadimos el style.css del tema padre
trasweb_add_css('/style.css', 'padre');
//después añadimos el style.css del tema hijo.
trasweb_add_css('/style.css', 'hijo');