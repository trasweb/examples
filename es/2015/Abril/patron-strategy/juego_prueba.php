<?php
include(__DIR__.'/Etiqueta.php');
?>
<!DOCTYPE html>
<html lang="es" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
</head>
<body>
<?php

$etiqueta = new Etiqueta('input');
$etiqueta->id = 'nombre';
$etiqueta->clase = 'trasweb';
$etiqueta->nombre = 'nombre';
$etiqueta->mostrar();

$etiqueta = new Etiqueta('select');
$etiqueta->id = 'ciudad';
$etiqueta->clase = 'trasweb';
$etiqueta->nombre = 'ciudad';
$etiqueta->opciones = ['Sevilla', 'Huelva', 'Córdoba', 'Málaga', 'Jaen', 'Almería', 'Cádiz', 'Granada' ];
$etiqueta->mostrar();



?>
</body>
</html>

