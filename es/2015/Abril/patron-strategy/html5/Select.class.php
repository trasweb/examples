<?php

class Select  {
 public function mostrar() {

   return function() {
	   $label = new $this('label');
	   $label->for = $this->id;
	   $label->texto = ucfirst($this->nombre);
	   $label->mostrar();
	  

		 echo "<select id='{$this->id}' name='{$this->nombre}'>";

		 foreach($this->opciones as $valor => $texto ) {
				 $option = new $this('option');
				 $option->valor = $valor;
				 $option->texto = $texto;
				 $option->mostrar();
		 }
		echo '</select>';
	};
 }

} 
