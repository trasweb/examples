<?php

class Input  {
 public function mostrar() {
   return function() {
	   $label = new $this('label');
	   $label->for = $this->id;
	   $label->texto = ucfirst($this->nombre);
	   $label->mostrar();
	  
	   echo "<input id='{$this->id}' class='{$this->clase}' name='{$this->nombre}' type='input' />";

	};
 }

} 
