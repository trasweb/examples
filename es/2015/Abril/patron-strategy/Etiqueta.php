<?php

define('DEFAULT_TAGS', 'html5');

class Etiqueta {

	protected $strategia;
	protected $atributos = array();
	
	public function __construct($etiqueta, $juego_etiquetas = DEFAULT_TAGS) {
		$nombre_clase_estrategia = ucfirst($etiqueta);
		$postfijo_clase = '.class.php';
		$ruta_a_etiquetas = __DIR__.'/'.$juego_etiquetas.'/';
		$clase = $ruta_a_etiquetas.$nombre_clase_estrategia.$postfijo_clase;
		
		$clase_existe = file_exists($clase);
		
		if($clase_existe) {
			require_once($clase);
			$this->estrategia = new $nombre_clase_estrategia();
		}else {
			throw new Exception('Error, clase '.$nombre_clase_estrategia.' no encontrada en '.$clase);
		}
	}

	public function mostrar() {
	
		$funcion_anonima_etiqueta = $this->estrategia->mostrar()->bindTo($this);
		$funcion_anonima_etiqueta();
	}


	public function __set($atributo, $valor) {
		$this->atributos[$atributo] = $valor;
	}

	public function __get($atributo) {
		$existe_atributo = isset($this->atributos[$atributo]);
	
		if($existe_atributo ) {
			return $this->atributos[$atributo];
		}else {
			return '';
		}
	}
	
}
